extern crate num_bigint;
extern crate num_traits;
extern crate rand;

use num_bigint::{BigUint, RandomBits};
use rand::Rng;
use std::fmt;
use std::fs;
use std::num;
#[allow(unused_imports)]
use std::io::{self, Read};

fn main() -> Result<(), CliError> {
    // Ideal scenario
    // alice makes a private key
    create_priv_key("alice_priv_key")?;

    // bob makes a private key
    create_priv_key("bob_priv_key")?;

    // alice creates a file named public key with her private key
    create_pub_key("alice_pub_key", "alice_priv_key")?;

    // bob creates a file named public key with his private key
    create_pub_key("bob_pub_key", "bob_priv_key")?;

    // session key used by alice
    create_session_key("alice_session_key", "alice_priv_key", "bob_pub_key")?;

    // session key used by bob 
    create_session_key("bob_session_key", "bob_priv_key", "alice_pub_key")?;
    let alice_pub_key = fs::read_to_string("alice_pub_key")?;
    let alice_priv_key = fs::read_to_string("alice_priv_key")?;
    let bob_pub_key = fs::read_to_string("bob_pub_key")?;
    let bob_priv_key = fs::read_to_string("bob_priv_key")?;
    let alice_session_key = fs::read_to_string("alice_session_key")?;
    let bob_session_key = fs::read_to_string("bob_session_key")?;
    println!("Alice's private key: {} \n\n\
             Bob's private key: {} \n\n\
             Alice's public key: {} \n\n\
             Bob's public key: {} \n\n\
             Alice's session key: {} \n\n\
             Bob's session key: {}", alice_priv_key, bob_priv_key,
             alice_pub_key, bob_pub_key, alice_session_key,
             bob_session_key);

    // Evil scenario, no need to recreate alice and bob's keys 
    // eve intercepts the traffic and replaces alice_pub_key with known value
    create_session_key("eve_alice_session_key", "alice_priv_key", "eve_pub_key")?;

    // eve intercepts the traffic and replaces bob_pub_key with known value
    create_session_key("eve_bob_session_key", "bob_priv_key", "eve_pub_key")?;
    let eve_alice_session_key = fs::read_to_string("eve_alice_session_key")?;
    let eve_bob_session_key = fs::read_to_string("eve_bob_session_key")?;

    println!("\n\t###Now Eve attacks###\n");
    println!("Alice's session key: {} \n\n\
             Bob's session key: {}", eve_alice_session_key,
             eve_bob_session_key);
             
    Ok(())
}


/// # Create a public key
/// Create a file in the working directory that contains the big num
/// representation of this user's public key. The function takes no
/// arguments, and returns the unit type upon success. If there is 
/// an error sanitizing the input, it is propogated upwards.
/// ## Example
/// ```
/// use std::fs;
///
///fn main() -> std::io::Result<()> {
///     match create_pub_key() {
///         Ok(()) => (), // the file pub_key has been created  
///         _ => panic!("Error generating public key")
///     }
///     let path = fs::canonicalize("priv_key")?;
///     Ok(())
/// }    
/// ```
fn create_pub_key(filename: &str, priv_key_filename: &str) -> Result<(), CliError> {
    // create the base point (must be primitive root modulo p)
    let g: Vec<u32> = vec![2;1]; // BigUint represents nums in radix 2^32
    let g: BigUint = BigUint::new(g);

    // define the modulus size p
    let p: BigUint = BigUint::from_bytes_le(
            "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc740\
             20bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374f\
             e1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee3\
             86bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da\
             48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed52\
             9077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff"
             .as_bytes());
    
    // take the previously generated private key and craft pub key from it
    let priv_key = sanitize_big_num(priv_key_filename)?;
    let pub_key = g.modpow(&priv_key, &p);
    fs::write(filename, format!("{:?}", pub_key))?;

    Ok(())
}


/// # Create a private key
/// Create a file in the working directory that contains the big num
/// representation of the this user's private key. The function takes
/// no arguments and should not fail provided these library calls are stable.
/// ## Example
/// ```
/// use std::fs;
///
/// fn main() -> std::io::Result<()> {
///     create_priv_key();
///     let path = fs::canonicalize("priv_key")?;
///     Ok(())
/// }
/// ```
fn create_priv_key(filename: &str) -> Result<(), CliError> {
    let mut rng = rand::thread_rng();
    let a: BigUint = rng.sample(RandomBits::new(32));
    fs::write(filename, format!("{:?}", a))?; 

    Ok(())
}


/// # Create a session key
/// Create a file in the working directory that contains the big num
/// representation of the these user's session key.
/// ## Example
/// ```
/// use std::fs;
///
/// fn main() -> std::io::Result<()> {
///     create_priv_key();
///     let path = fs::canonicalize("priv_key")?;
///     Ok(())
/// }
/// ```
fn create_session_key(filename: &str, priv_key: &str, pub_key: &str) -> Result<(), CliError> { 
    // need another copy of mod 
    let p: BigUint = BigUint::from_bytes_le(
            "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc740\
             20bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374f\
             e1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee3\
             86bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da\
             48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed52\
             9077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff"
             .as_bytes());

    // note that session_key file contains other person's public key
    // take this client's public key from the file
    let priv_key = sanitize_big_num(priv_key)?;
    let pub_key = sanitize_big_num(pub_key)?;
    let session_key = pub_key.modpow(&priv_key, &p);
    fs::write(filename, format!("{:?}", session_key))?;

    Ok(())
}


/// # Sanitize a big num from file
/// The keys are stored as files in the working directory. However, they're
/// not stored as base 10 numbers, rather their big num representation is in 
/// 2^32. So this file is stripped of its excess characters, and piped into 
/// a BigUint appropriately.
/// ## Example
/// ```
/// unimplemented!()
/// ```
fn sanitize_big_num(filename: &str) -> Result <BigUint, CliError> { 
    // takes in a filename and reads the contents of the file to a string
    let mut raw_data = fs::read_to_string(filename)?;
    
    // strips the contents that are not 0-9 or whitespace
    raw_data.retain(|c| c == ' ' || c == '0' || c == '1' ||
                        c == '2' || c == '3' || c == '4' ||
                        c == '5' || c == '6' || c == '7' ||
                        c == '8' || c == '9');

    // take each of those chunks and plop it in an element of Vec<u32>
    let split_data = raw_data.split_whitespace();

    // prepare a data structure to store the parsed data
    let mut parsed_data: Vec<u32> = vec![];

    // split the chunks and error out if unsuccessful
    for chunk in split_data {
        parsed_data.push(chunk.parse::<u32>()?);
    }
    
    // create the desired bignum from the vector
    let bignum = BigUint::new(parsed_data);

    Ok(bignum)
}


// Code taken from here.
// https://doc.rust-lang.org/std/convert/trait.From.html
// Inspiration from here and Rust-By-Example
// https://stackoverflow.com/questions/42584368/how-do-you-define-custom-error-types-in-rust
#[derive(Debug)]
enum CliError {
    IoError(io::Error),
    ParseError(num::ParseIntError)
}

impl fmt::Display for CliError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "print your error msg here")
    }
}

// Need to impl From so these errors can be used with `?`
impl From<io::Error> for CliError {
    fn from(error: io::Error) -> Self {
        CliError::IoError(error)
    }
}

impl From<num::ParseIntError> for CliError {
    fn from(error: num::ParseIntError) -> Self {
        CliError::ParseError(error)
    }
}
